﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc.Html;
using System.Xml;
using DotNET_Developer_Technical_Test.Models;

namespace DotNET_Developer_Technical_Test
{
    public class NewsFeedGenerator
    {
        public NewsFeed GenerateNewFeed()
        {
            var tempNewsFeed = GetXMLData();

            return new NewsFeed()
            {
                newsFeedItems = tempNewsFeed
            };
        }

        private List<NewsFeedItem> GetXMLData()
        {
            var tempNewsFeed = new List<NewsFeedItem>();

            if (File.Exists(HostingEnvironment.MapPath("~/Content/NewsFeedData.xml")))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(HostingEnvironment.MapPath("~/Content/NewsFeedData.xml"));

                foreach (XmlNode node in doc.ChildNodes)
                {
                    if (string.Equals(node.Name, "NewsFeed"))
                    {
                        foreach (XmlNode childNode in node.ChildNodes)
                        {
                            string id = childNode.ChildNodes[0].InnerText;
                            string title = childNode.ChildNodes[1].InnerText;
                            string imageUrl = childNode.ChildNodes[2].InnerText;
                            string imageText = childNode.ChildNodes[3].InnerText;
                            string linkURL = childNode.ChildNodes[4].InnerText;
                            string briefDescription = childNode.ChildNodes[5].InnerText;
                            string story = childNode.ChildNodes[6].InnerText;

                            tempNewsFeed.Add(new NewsFeedItem()
                            {
                                Id = id,
                                Title = title,
                                ImageURL = imageUrl,
                                ImageText = imageText,
                                LinkURL = linkURL,
                                BriefDescription = briefDescription,
                                Story = story
                            });
                        }
                    }

                }
            }

            return tempNewsFeed;
        }

        public NewsFeedItem GetNewsFeedItem(string newsItem)
        {
            var tempNewsFeed = GetXMLData();

            foreach (var item in tempNewsFeed)
            {
                if(item.Id == newsItem)
                {
                    return item;
                }
            }

            return null;
        }
    } 
}