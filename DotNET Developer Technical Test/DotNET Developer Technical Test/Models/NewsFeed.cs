﻿using System.Collections.Generic;

namespace DotNET_Developer_Technical_Test.Models
{
    public class NewsFeed
    {
        public int itemsToShow;
        public List<NewsFeedItem> newsFeedItems;
    }
}