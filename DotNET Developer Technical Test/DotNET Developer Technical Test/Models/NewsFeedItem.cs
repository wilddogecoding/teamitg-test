﻿using System.Collections.Generic;

namespace DotNET_Developer_Technical_Test.Models
{
    public class NewsFeedItem
    {
        public string Id;
        public string Title;
        public string ImageURL;
        public string ImageText;
        public string LinkURL;
        public string BriefDescription;
        public string Story;
    }
}