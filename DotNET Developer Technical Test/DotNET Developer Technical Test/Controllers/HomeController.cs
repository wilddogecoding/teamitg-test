﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DotNET_Developer_Technical_Test.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var generator = new NewsFeedGenerator();

            var newsFeed = generator.GenerateNewFeed();

            newsFeed.itemsToShow = 5;

            return View(newsFeed);
        }

        public ActionResult Contact()
        {
            ViewBag.Title = "Contact";
            ViewBag.Message = "Thank you for your consideration";

            return View();
        }

        public ActionResult NewsStory(string newsItem)
        {
            var generator = new NewsFeedGenerator();

            var model = generator.GetNewsFeedItem(newsItem);

            return View(model);
        }
    }
}