﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DotNET_Developer_Technical_Test.Controllers
{
    public class ActionController : Controller
    {
        public ActionResult LoadMoreNewsFeed(int itemsToShow)
        {
            var generator = new NewsFeedGenerator();
            var model = generator.GenerateNewFeed();

            model.itemsToShow = itemsToShow;

            return PartialView("_NewsFeed", model);
        }
    }
}